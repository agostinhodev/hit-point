@include('components.header.header')

@include('components.navbar.navbar')

<div class="container-fluid">

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Sem permissões</h1>

    </div>

    <div class="text-center">

        <div class="alert alert-danger">

            <strong>Você não tem permissões para acessar este conteúdo.</strong>

        </div>

    </div>



@include('components.footer.footer')
