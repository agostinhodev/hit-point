<div class="container">

    <button
        type="button"
        id="botaoAbrirModal"
        data-backdrop="static"
        class="btn btn-info btn-lg"
        data-toggle="modal"
        style="display:none;"
        data-target="#modal">Abrir</button>

    <div class="modal fade" id="modal" role="dialog" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div id="containerTituloModal"></div>
                </div>
                <div class="modal-body">
                    <div id="containerTextoModal" style="text-align:center;"></div>
                </div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-danger"
                        data-dismiss="modal"
                        id="btnFecharModal"
                        onclick="modal.fechar()"

                    ><i class="fa fa-close"></i>&nbsp;Fechar</button>
                </div>
            </div>
            <input type="hidden" id="estaAberta" value="0">
        </div>
    </div>

</div>
