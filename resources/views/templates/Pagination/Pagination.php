<script id="templatePagination" type="x-handlebars-template">

    <nav>
        <ul class="pagination">

            {{#if primeira}}
                <li class="page-item"><a
                        onclick="{{nomeClasse}}.setPagina({{primeira}}, true)"
                        class="page-link"
                        href="#">Primeira</a></li>
            {{/if}}

            {{#if anterior}}
                <li class="page-item"><a
                        onclick="{{nomeClasse}}.setPagina({{anterior}}, true)"
                        class="page-link"
                        href="#">{{anterior}}</a></li>
            {{/if}}

                <li class="page-item active"><a class="page-link" href="#">{{atual}}</a></li>

            {{#if proxima}}
                <li class="page-item"><a
                        onclick="{{nomeClasse}}.setPagina({{proxima}}, true)"
                        class="page-link"
                        href="#">{{proxima}}</a></li>
            {{/if}}

            {{#if ultima}}
                <li class="page-item"><a
                        onclick="{{nomeClasse}}.setPagina({{ultima}}, true)"
                        class="page-link"
                        href="#">Última</a></li>
            {{/if}}

        </ul>
    </nav>

</script>
