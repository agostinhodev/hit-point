<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Illuminate\Support\Facades\DB;

class CreateClockTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clock_types', function (Blueprint $table) {

            $table->id();
            $table->string('description', 100)->unique();

        });

        DB::table('clock_types')->insert(
            [
                ['description' => 'ENTRADA NO SERVIÇO'],
                ['description' => 'SAÍDA PARA ALMOÇO'],
                ['description' => 'RETORNO DO ALMOÇO'],
                ['description' => 'FIM DE EXPEDIENTE'],
                ['description' => 'EXCEPCIONAL: SAÍDA PARA OUTROS FINS'],
                ['description' => 'EXCEPCIONAL: RETORNO DE OUTROS FINS'],

            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clock_types');
    }
}
