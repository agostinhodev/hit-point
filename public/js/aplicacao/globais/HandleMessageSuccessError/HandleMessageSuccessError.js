class HandleMessageSuccessError{

    constructor(){

        this.template      = document.getElementById('templateMessageSuccessError');
        this.type          = null;
        this.container     = null;
        this.message       = null;
        this.json          = {};
        this.generatedHTML = null;

    }

    setType(type){

        //de acordo com o alert do bootstrap

        this.type = type;

    }

    setContainer(container){

        this.container = container;


    }

    setMessage(message){

        this.message = message;

    }

    mountJson(){

        this.json = {

            type: this.type,
            message: this.message

        }

    }

    write(){

        if(this.container.id === 'containerTextoModal'){

            modal.abrir("Atenção:", this.generatedHTML);
            modal.mostrarOcultarBtnFechar(1);

        } else {

            this.container.innerHTML = this.generatedHTML;

        }

    }

    generateHTML(){

        let templateCompilado = Handlebars.compile(this.template.innerHTML);
        this.generatedHTML    = templateCompilado(this.json);

    }

    show(){

        this.mountJson();
        this.generateHTML();
        this.write();

    }

}
