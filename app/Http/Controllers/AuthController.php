<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use ReCaptcha\ReCaptcha;

class AuthController extends Controller
{
    public function dashboard()
    {

        if(Auth::check()){

            $name = auth()->user()->name;

            return view('pages.InSide.Home.home', [

                'name' => $name

            ]);


        }

        return redirect()->route('login');

    }

    public function showLoginPage()
    {
        return view('pages.OutSide.Login.login');

    }

    public function handleLogin( Request $request )
    {

        if(!isset($request->google_recaptcha_token) || strlen($request->google_recaptcha_token) === 0)
            return redirect()->back()->withInput()->withErrors([
                'É necessário enviar o token do Google Recaptcha'
            ]);

        if(!isset($request->email) || strlen($request->email) === 0)
            return redirect()->back()->withInput()->withErrors([
                'É necessário informar o email corretamente'
            ]);

        if(!isset($request->password) || strlen($request->password) === 0)
            return redirect()->back()->withInput()->withErrors([
                'É necessário informar a senha corretamente'
            ]);

        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL))
            return redirect()->back()->withInput()->withErrors([
                'O email informado é inválido'
            ]);

        /*Não é uma boa prática deixar a secret key do recaptcha aqui,
        porém como se trata de um projeto demonstrativo, está tudo bem!*/

        $recaptcha = new ReCaptcha('6LckeOQZAAAAANf2IEh3LB2_1UNIAdNqj_WCAzK1');
        //Apenas para teste
        //$respReccaptcha = $recaptcha->verify($request->google_recaptcha_token);

        if (false)
            return redirect()->back()->withInput()->withErrors([
                'Não foi possível validar o token do recaptcha'
            ]);

        //Considera apenas os usuários ativos

        $responseLogin = Auth::attempt([

            'email' => $request->email,
            'password' => $request->password,
            'isDeleted' => 0

        ]);

        if($responseLogin){

            return redirect()->route('dashboard');

        } else {

            return redirect()->back()->withInput()->withErrors([

                'Email ou senha incorretos'

            ]);

        }

    }

    public function handleLogout()
    {

        Auth::logout();
        return redirect()->route('login');

    }

}
