class Address{

    validateCEP( input ){

        let description = document.getElementById('inputDescription');
        let number = document.getElementById('inputNumber');
        let complement = document.getElementById('inputComplement');
        let neighborhood = document.getElementById('inputNeighborhood');
        let city = document.getElementById('inputCity');
        let uf = document.getElementById('inputUf');

        if(input.value.length === 9){

            input.disabled = true;
            input.style.cursor = 'wait';

            let cep = input.value.replace(/\D/g, '');

            axios.get(`https://viacep.com.br/ws/${cep}/json/`)

                .then((response)=>{

                    if(
                        typeof response !== 'undefined' &&
                        typeof response.data !== 'undefined'
                    ){

                        if(typeof response.data.erro === 'undefined') {

                            description.value = response.data.logradouro;
                            complement.value = response.data.complemento;
                            neighborhood.value = response.data.bairro;

                            city.value = response.data.localidade;
                            uf.value = response.data.uf;

                            description.disabled = false;
                            number.disabled = false;
                            complement.disabled = false;
                            neighborhood.disabled = false;
                            city.disabled = false;
                            uf.disabled = false

                        } else {

                            let erro = new HandleMessageSuccessError();
                            erro.setType('danger');
                            erro.setContainer(modal.containerTexto);
                            erro.setMessage(`O CEP <strong>${input.value}</strong> é inválido`);
                            erro.show();

                            input.value = "";


                        }

                    }

                })

                .catch((error)=>{
                    console.log(error)
                })

            input.disabled = false;
            input.style.cursor = '';

        } else {

            description.value = '';
            complement.value = '';
            neighborhood.value = '';
            city.value = '';
            number.value = '';
            uf.value = '';

            description.disabled = true;
            complement.disabled = true;
            neighborhood.disabled = true;
            city.disabled = true;
            number.disabled = true;
            uf.disabled = true;


        }

    }

}

const address = new Address();
