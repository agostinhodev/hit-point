<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\AuthController;
use \App\Http\Controllers\ClockController;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\Notices;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function (){

    return redirect()->route('dashboard');

});

Route::get('/dashboard', [AuthController::class, 'dashboard'])->name('dashboard');
Route::get('/login', [AuthController::class, 'showLoginPage'])->name('login');
Route::post('/login/process', [AuthController::class, 'handleLogin'])->name('login.handleProcess');
Route::get('/logout', [AuthController::class, 'handleLogout'])->name('handleLogout');

Route::get('/clock', [ClockController::class, 'showFormRegister'])->name('clock.showFormRegister');
Route::get('/clock/audit', [ClockController::class, 'showAudit'])->name('clock.showAudit');
Route::post('/clock/save', [ClockController::class, 'save'])->name('clock.saveNew');

Route::get('/user/info', [UserController::class, 'showInfo'])->name('user.showInfo');
Route::post('/user/update-password', [UserController::class, 'updatePassword'])->name('user.updatePassword');
Route::get('/users', [UserController::class, 'getAllUsers'])->name('user.showAll');
Route::delete('/user/remove', [UserController::class, 'delete'])->name('user.delete');
Route::get('/user/{id_user}', [UserController::class, 'showEditForm'])->name('user.showEditForm');
Route::put('/user/{id_user}', [UserController::class, 'handleUpdate'])->name('user.handleUpdate');
Route::get('/users/new', [UserController::class, 'showFormNew'])->name('user.showFormNew');
Route::post('/user/new', [UserController::class, 'handleNew'])->name('user.handleNew');

//Notices
Route::get('/notice/no-permission', [Notices::class, 'showHasNoPermission'])->name('notices.hasNoPermission');



