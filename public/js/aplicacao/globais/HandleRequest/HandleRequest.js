class HandleRequest{

    constructor(){

        this.url             = document.getElementById('_route').value;
        this.method          = null;
        this.req             = null;
        this.headers         = null;
        this.params          = null;
        this.axios           = null;
        this.response        = null;
        this.responseType    = null;
        this.template        = null;
        this.container       = null;
        this.callbackError   = null;
        this.callbackSuccess = null;

    }

    validateParams(config){

        if(typeof config !== 'object')
            throw new Error("O parâmetro config precisa ser um objeto")

        if(typeof config.headers !== 'object')
            throw new Error("A propriedade config.headers precisa ser um objeto");

        if(
            typeof config.headers.route === "undefined" ||
            config.headers.route === '' ||
            config.headers.route === null
        )throw new Error("A propriedade route é inválida")

        if(typeof config.params !== 'object')
            throw new Error("A propriedade config.params precisa ser um objeto");

        if(
            typeof config.method !== "string" ||
            (
                config.method !== "GET" &&
                config.method !== "POST" &&
                config.method !== "PUT"
            )
        )throw new Error("A propriedade method precisa ser GET ou POST")

        if(typeof config.responseType !== 'string')
            throw new Error("É necessário informar o tipo de tratamento de resposta para esta requisição");

        if(typeof config.responseType === "contentMessage"){

            if(
                typeof config.template === "undefined" ||
                config.template === '' ||
                config.template === null
            )throw new Error("A propriedade template é inválida")

        } else if(typeof config.responseType !== "contentMessage"){

            this.template = null;

        } else {

            throw new Error("O tipo de tratamento de resposta é inválido")

        }

        if(
            typeof config.container === "undefined" ||
            config.container === '' ||
            config.container === null
        )throw new Error("A propriedade container é inválida")

        if(typeof config.callbackError === "undefined" || config.callbackError === null)
            config.callbackError = null;

        if(typeof config.callbackSuccess === "undefined" || config.callbackSuccess === null)
            config.callbackSuccess = null;

    }

    setMethod(method){
        this.method = method;
    }

    processRequest(){

        switch(this.method){

            case 'GET':

                this.req = this.axios.get(this.url, {

                    params:this.params

                });

            break;

            case 'POST':

                this.req = this.axios.post(this.url, this.params);

            break;

            case 'PUT':

                this.req = this.axios.put( this.url, this.params );

            break;

            default:

                throw new Error( 'Método HTTP não reconhecido como válido para esta requisição' );

            break;

        }

    }

    processResponse(){

        this.req.then((response)=>{

            this.response = response.data;

            if(
                typeof this.response === 'undefined' ||
                this.response.status === false
            )throw new Error(this.response.message);

            this.handleSuccess();

        })
        .catch((error)=>{

            if(
                typeof error !== 'undefined' &&
                typeof error.response !== 'undefined' &&
                typeof error.response.data !== 'undefined' &&
                typeof error.response.data.message !== 'undefined'
            )
                this.handleError( error.response.data.message );
            else
                this.handleError(error.message);

        });

    }

    handleSuccess(){

        const handleLoading = new HandleLoading();
        handleLoading.setContainer(this.container);
        handleLoading.hide();

        if(this.responseType === 'contentMessage'){

            //Se o tipo de resposta for um array, ou algo do tipo, trata como um template independente

            let templateBruto        = this.template.innerHTML;
            let templateCompilado    = Handlebars.compile(templateBruto);

            Handlebars.registerHelper('compare', function(lvalue, rvalue, options) {

                if (arguments.length < 3)
                    throw new Error("Handlerbars Helper 'compare' needs 2 parameters");

                const operator = (typeof options.hash.operator === "undefined") ? '==' :  options.hash.operator;

                var operators = {
                    '==':       function(l,r) { return l == r; },
                    '===':      function(l,r) { return l === r; },
                    '!=':       function(l,r) { return l != r; },
                    '<':        function(l,r) { return l < r; },
                    '>':        function(l,r) { return l > r; },
                    '<=':       function(l,r) { return l <= r; },
                    '>=':       function(l,r) { return l >= r; },
                    'typeof':   function(l,r) { return typeof l == r; }
                };

                if (!operators[operator])
                    throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);

                var result = false;

                if (rvalue.indexOf("|") != -1) {

                    var values = rvalue.split('|');
                    var matches = 0;

                    for (var i = 0, j = values.length; i < j; i++) {

                        if(operators[operator](lvalue,values[i])){ matches++; }

                    }
                    result = (matches > 0);

                } else {

                    result = operators[operator](lvalue,rvalue);

                }

                if( result ) {

                    return options.fn(this);

                } else {

                    return options.inverse(this);

                }

            });

            Handlebars.registerHelper('DataBDparaDataBR', function(value){
                let formatacao = new Formatacao();
                return formatacao.dataBDparaDataBR(value);
            });

            Handlebars.registerHelper( 'valorUSDparaBRL', function (valor) {
                let formatacao = new Formatacao();
                return formatacao.valorUSDparaBRL( valor );
            });

            Handlebars.registerHelper( 'convertCEPBDtoBR', function (cep) {
                let formatacao = new Formatacao();
                return formatacao.convertCEPBDtoBR( cep );
            });

            Handlebars.registerHelper('handlePagination', function (
                infoPagination,
                container,
                nomeClasse
            ) {

                let handlePagination = new HandlePagination();
                handlePagination.setContainer( container );
                handlePagination.setContent( infoPagination );
                handlePagination.setNomeClasse( nomeClasse );
                handlePagination.show();

            });

            let HTMLGerado           = templateCompilado(this.response);
            this.container.innerHTML = HTMLGerado;

        } else {

            const handleMessageSuccessError = new HandleMessageSuccessError();
            handleMessageSuccessError.setType('success');
            handleMessageSuccessError.setContainer(this.container);
            handleMessageSuccessError.setMessage(this.response.message);
            handleMessageSuccessError.show();

        }

        if(this.callbackSuccess !== null){

            this.callbackSuccess();
            this.callbackSuccess = null;

        }

        if(this.container.id === modal.containerTexto.id){

            modal.setTitulo('');

        }


    }

    handleError(message){

        const handleLoading = new HandleLoading();
        handleLoading.setContainer(this.container);
        handleLoading.hide();

        const handleMessageSuccessError = new HandleMessageSuccessError();
        handleMessageSuccessError.setType('danger');
        handleMessageSuccessError.setContainer(this.container);
        handleMessageSuccessError.setMessage(message);
        handleMessageSuccessError.show();

        if(this.callbackError !== null){

            this.callbackError();
            this.callbackError = null;

        }

    }

    setHeaders(headers){

        this.headers = headers;

    }

    setParams(params){

        this.params = params;

    }

    setTemplate(template){

        this.template = document.getElementById(template);

    }

    setContainer(container){

        if(
            typeof container === 'undefined' ||
            container === '' ||
            container === null
        ){

            this.container = document.getElementById(modal.containerTexto.id);
            throw new Error( 'Informe o container da resposta corretamente' );

        }

        this.container = document.getElementById(container);

    }

    setCallBackError(callbackError){

        if(callbackError !== null)
            this.callbackError = callbackError;

    }

    setCallBackSuccess(callback){

        if(callback !== null)
            this.callbackSuccess = callback;

    }

    setResponseType(responseType){

        this.responseType = responseType;

    }

    instanceAxios(){

       this.axios = axios.create({

            headers: this.headers

       });

    }

    handleLoading(){

        const handleLoading = new HandleLoading();
        handleLoading.process(this.container);

    }

    process(config){

        try{

            this.setContainer(config.container);
            this.validateParams(config);
            this.setMethod(config.method);
            this.setHeaders(config.headers);
            this.setParams(config.params);
            this.setTemplate(config.template);
            this.setCallBackError(config.callbackError);
            this.setCallBackSuccess(config.callbackSuccess);
            this.setResponseType(config.responseType);

            this.handleLoading();

            this.instanceAxios();
            this.processRequest();
            this.processResponse();

        } catch(e){

            this.handleError(e.message);

        }

    }

}
