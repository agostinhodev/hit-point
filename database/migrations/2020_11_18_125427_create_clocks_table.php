<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clocks', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('user')->unsigned();
            $table->bigInteger('type')->unsigned();
            $table->timestamp('created_at')->useCurrent();
            $table->text('notes')
                ->nullable()
                ->comment('Caso o funcionário queira deixar uma justificativa da sua saída. Não é obrigatório, entretanto caso ele saia mais cedo, será cobrado posteriormente');

            $table->foreign('user')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT')
                ->onUpdate('CASCADE');

            $table->foreign('type')
                ->references('id')
                ->on('clock_types')
                ->onDelete('RESTRICT')
                ->onUpdate('CASCADE');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clocks');
    }
}
