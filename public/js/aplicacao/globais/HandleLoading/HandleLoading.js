class HandleLoading{

    constructor(){

        this.container = null;
        this.template  = document.getElementById("templateLoading").innerHTML;
       
    }

    setContainer(container){
        
        this.container = container;

    }

    handleError(message){

        alert(message);

    }

    show(){
   
        let templateCompilado    = Handlebars.compile(this.template);	
        let HTMLGerado           = templateCompilado();        
        this.container.innerHTML = HTMLGerado;

    }

    hide(){

        this.container.innerHTML = "";

    }

    process(container){

        try{

            this.setContainer(container);
            this.show();

        } catch(e){

            this.handleError(e.message);

        }

    }

}