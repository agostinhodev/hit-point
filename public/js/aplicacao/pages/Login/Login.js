class Login{

    constructor() {

        this.credencial = document.getElementById('credencial');
        this.senha = document.getElementById('senha');
        this.token = document.getElementById('token');
        this.btnLogin = document.getElementById('btnLogin');
        this.containerCarregando = document.getElementById('containerCarregando');

        this.credencial.addEventListener('click', ()=>{

            login.containerCarregando.innerHTML = '';

        });

        this.senha.addEventListener('click', ()=>{

            login.containerCarregando.innerHTML = '';

        });

        this._token = document.getElementsByName('_token')[0];

    }

    habilitarDesabilitarFormLogin( acao = false ){

        if(acao){

            this.credencial.disabled = true;
            this.senha.disabled = true;
            this.btnLogin.disabled = true;

        } else{

            this.credencial.disabled = false;
            this.senha.disabled = false;
            this.btnLogin.disabled = false;

        }

    }

    processar(){

        let handleRequest = new HandleRequest();
        this.habilitarDesabilitarFormLogin(true);

        handleRequest.process({

            headers:{
                route: 'usuario/login'
            },
            params: {

                _token: this._token.value,
                credencial: this.credencial.value,
                senha: this.senha.value,
                token: this.token.value

            },
            method: 'POST',
            responseType: 'onlySuccessOrErrorMessage',
            template: 'containerTextoModal',
            container: this.containerCarregando.id,
            callbackSuccess: ()=>{

                location.href = '/';

            },
            callbackError: ()=>{

                login.habilitarDesabilitarFormLogin(false);
                getGoogleRecaptchaToken();

            }

        });

        return false;

    }

}

const login = new Login();
