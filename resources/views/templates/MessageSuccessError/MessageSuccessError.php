<script id="templateMessageSuccessError" type="text/x-handlebars-template">

    <center>

        <div class="alert alert-{{type}}">
            {{{message}}}
        </div>

    </center>

</script>
