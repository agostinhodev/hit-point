<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClockTypes extends Model
{
    use HasFactory;

    protected $table = 'clock_types';

}
