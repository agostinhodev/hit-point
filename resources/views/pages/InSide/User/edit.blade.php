@include('components.header.header')

@include('components.navbar.navbar')

<div class="container-fluid">

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Editar Informações do Usuário</h1>

    </div>

    @if($errors->all() )

        @foreach( $errors->all() as $error )

            <div class="alert alert-danger text-center">
                {{ $error  }}
            </div>

        @endforeach

    @elseif( session()->has('success') )

        <div class="alert alert-success text-center">
            {{ session()->get('success') }}
        </div>

    @endif

    <form action="{{ route('user.handleUpdate', ['id_user' => $user->id]) }}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="inputName">Nome</label>
            <input
                type="text"
                class="form-control"
                id="inputName"
                aria-describedby="nameHelp"
                placeholder="Digite o nome do funcionário"
                value="{{$user->name}}"
                name="name"
            >
            <small id="nameHelp" class="form-text text-muted">O nome deve ser completo, contento nome e sobrenome</small>
        </div>

        <div class="form-group">
            <label for="inputCPF">CPF:</label>
            <input
                type="text"
                class="form-control cpf"
                id="inputCPF"
                aria-describedby="cpfHelp"
                placeholder="Digite o CPF do funcionário"
                value="{{ \App\Http\Controllers\Validation::mask( $user->cpf, '###.###.###-##' )}}"
                name="cpf"
                required
            >
            <small id="cpfHelp" class="form-text text-muted">Ex: 123.456.789-10</small>
        </div>

        <div class="form-group">
            <label for="inputCPF">E-mail:</label>
            <input
                type="text"
                class="form-control"
                id="inputEmail"
                aria-describedby="emailHelp"
                placeholder="Digite o Email do funcionário"
                value="{{$user->email}}"
                name="email"
                required
            >
            <small id="emailHelp" class="form-text text-muted">Ex: exemplo@ticto.com.br</small>
        </div>

        <div class="form-group">
            <label for="inputOccupation">Cargo:</label>

            <select class="form-control" name="occupation" required>
                @foreach( $occupations as $occupation )

                    @if( $occupation->id == $user->occupation )

                        <option value="{{$occupation->id}}" selected> {{$occupation->description}} (Atual)</option>

                    @else

                        <option value="{{$occupation->id}}"> {{$occupation->description}} </option>

                    @endif

                @endforeach

            </select>

            <small id="cargoHelp" class="form-text text-muted">Ex: exemplo@ticto.com.br</small>
        </div>

        <div class="form-group">
            <label for="inputBirthday">Data de Nascimento:</label>
            <input
                type="date"
                class="form-control"
                id="inputBirthday"
                aria-describedby="birthdayHelp"
                placeholder="Digite o Email do funcionário"
                value="{{ $user->birthday }}"
                name="birthday"
                required
            >
            <small id="birthdayHelp" class="form-text text-muted">Ex: exemplo@ticto.com.br</small>
        </div>

        <div class="form-group">
            <input type="hidden" name="address_id" value="{{$address->id}}">
            <label for="inputCEP">CEP:</label>
            <input
                type="text"
                class="form-control cep"
                id="inputCEP"
                aria-describedby="cepHelp"
                placeholder="Digite o CEP"
                name="cep"
                required
                value="{{ \App\Http\Controllers\Validation::mask($address->cep, '#####-###') }}"
            >
            <small id="cepHelp" class="form-text text-muted">Ex: 39801-500</small>
        </div>

        <div class="form-group">
            <label for="inputDescription">Logradouro:</label>
            <input
                type="text"
                class="form-control"
                id="inputDescription"
                aria-describedby="descriptionHelp"
                placeholder="Digite o nome do logradouro"
                name="description"
                required
                value="{{ $address->description }}"

            >
            <small id="descriptionHelp" class="form-text text-muted">Ex: Av. dos Laravel Lovers</small>
        </div>

        <div class="form-group">
            <label for="inputNumber">Número:</label>
            <input
                type="text"
                class="form-control"
                id="inputNumber"
                aria-describedby="numberHelp"
                placeholder="Digite o número"
                name="number"
                required
                value="{{ $address->number }}"


            >
            <small id="numberHelp" class="form-text text-muted">Ex: 2374, AP. 301</small>
        </div>

        <div class="form-group">
            <label for="inputComplement">Complemento:</label>
            <input
                type="text"
                class="form-control"
                id="inputComplement"
                aria-describedby="complementHelp"
                placeholder="Digite o Complemento"
                name="complement"
                required
                value="{{ $address->complement }}"


            >
            <small id="complementHelp" class="form-text text-muted">Ex: Próximo a Rua dos Javeiros :)</small>
        </div>

        <div class="form-group">
            <label for="inputNeighborhood">Bairro:</label>
            <input
                type="text"
                class="form-control"
                id="inputNeighborhood"
                aria-describedby="neighborhoodHelp"
                placeholder="Digite o Complemento"
                name="neighborhood"
                required
                value="{{ $address->neighborhood }}"


            >
            <small id="neighborhoodHelp" class="form-text text-muted">Ex: Bairro do Ticto :)</small>
        </div>

        <div class="form-group">
            <label for="inputCity">Cidade:</label>
            <input
                type="text"
                class="form-control"
                id="inputCity"
                aria-describedby="cityHelp"
                placeholder="Digite o nome da Cidade"
                name="city"
                required
                value="{{ $address->city }}"


            >
            <small id="cityHelp" class="form-text text-muted">Ex: Cidade do PHP</small>
        </div>

        <div class="form-group">
            <label for="inputUf">UF:</label>
            <input
                type="text"
                class="form-control"
                id="inputUf"
                aria-describedby="ufHelp"
                placeholder="Digite a UF"
                name="uf"
                required
                value="{{ $address->uf }}"
            >
            <small id="ufHelp" class="form-text text-muted">Ex: MG / SP / RJ / RN / RS</small>
        </div>


        <div class="text-center">
            <button type="submit" class="btn btn-primary">
                <i class="fas fa-fw fa-save"></i>&nbsp;Salvar
            </button>
        </div>
    </form>

    <br>

</div>

@include('components.footer.footer')
