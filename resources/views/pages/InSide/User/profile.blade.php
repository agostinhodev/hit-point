@include('components.header.header')

@include('components.navbar.navbar')

<link rel="stylesheet" href="{{ asset('css/aplicacao/profile.css') }}">

<div class="container-fluid">

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Perfil do Usuário</h1>

    </div>

    <div class="container">
        <div class="row profile">
            <div class="col-md-3">
                <div class="profile-sidebar">

                    <div class="text-center">
                        <div class="profile-userpic">
                            <img src="{{asset('img/user.png')}}" class="img-responsive" alt="">
                        </div>
                    </div>

                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            {{ $user->name }}
                        </div>
                        <div class="profile-usertitle-job">
                            {{ $job }}
                        </div>

                        <div class="profile-usertitle-job">
                            {{ $user->email }}
                        </div>

                        <div class="profile-usertitle-name">

                            @if($user->level == 0)

                                <span class="badge badge-primary">Colaborador</span>

                            @else

                                <span class="badge badge-success">Administrador</span>

                            @endif
                        </div>

                    </div>

                </div>
            </div>
            <div class="col-md-9">
                <div class="profile-content">

                    <form action="{{ route('user.updatePassword') }}"  method="POST">

                        @csrf

                        <div class="form-group">
                            <label for="inputActualPassword">Senha Atual</label>
                            <input
                                type="password"
                                class="form-control"
                                id="inputActualPassword"
                                placeholder="Digite a sua senha atual"
                                name="currentPassword"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="inputNewPassword">Nova Senha</label>
                            <input
                                type="password"
                                class="form-control"
                                id="inputNewPassword"
                                placeholder="Escolha uma nova senha"
                                name="newPassword"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="inputConfirmPassword">Confirme a nova senha</label>
                            <input
                                type="password"
                                class="form-control"
                                id="inputConfirmPassword"
                                placeholder="Confirme a senha escolhida"
                                name="newPasswordConfirmation"
                                required>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Alterar Senha</button>

                        </div>
                    </form>

                    <br>
                    @if($errors->all() )

                        @foreach( $errors->all() as $error )

                            <div class="alert alert-danger text-center">
                                {{ $error  }}
                            </div>

                        @endforeach

                    @elseif( session()->has('success') )

                        <div class="alert alert-success text-center">
                            {{ session()->get('success') }}
                        </div>

                    @endif

                </div>
            </div>
        </div>
    </div>

</div>

@include('components.footer.footer')
