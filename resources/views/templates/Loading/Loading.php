<script id="templateLoading" type="x-handlebars-template">

    <center>
        <div class="d-flex justify-content-center">
            <div class="spinner-border" role="status"></div>
        </div>

        <strong>{{message}}</strong>
    </center>

</script>
