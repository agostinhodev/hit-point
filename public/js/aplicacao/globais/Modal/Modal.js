class Modal{

    constructor(){

        this.estaAberta      = document.getElementById("estaAberta");
        this.botaoAbrir      = document.getElementById("botaoAbrirModal");
        this.containerTitulo = document.getElementById("containerTituloModal");
        this.containerTexto  = document.getElementById("containerTextoModal");

    }

    mudarTamanho(tamanho){

        document.getElementsByClassName("modal-dialog")[0].style.minWidth = tamanho;
    
    }

    abrir(titulo, texto){
        
        this.setTitulo(titulo);
        
        this.containerTexto.innerHTML  = texto;
    
        if(this.estaAberta.value == 0){
    
            this.mostrarOcultarBtnFechar(1);
            this.botaoAbrir.click();
            this.estaAberta.value = 1;		
    
        }
    
    }

    fechar(){

        this.mudarTamanho('auto');
            
        this.containerTitulo.innerHTML = "";
        this.containerTexto.innerHTML  = "";
    
        if(this.estaAberta.value == 1){
            
            this.botaoAbrir.click();
            this.estaAberta.value = 0;
    
        }
            
    }
    
    mostrarOcultarBtnFechar(opcao){

        if(opcao == 0){
    
            $("#btnFecharModal").hide();
    
        } else if(opcao == 1){
    
            $("#btnFecharModal").show();
    
        }
    
    }

    setTitulo(titulo){

        this.containerTitulo.innerHTML = "<strong>" + titulo + "</strong>";

    }

}

const modal = new Modal();