@include('components.header.header')

@include('components.navbar.navbar')

<div class="container-fluid">

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Cadastrar novo Funcionário:</h1>

    </div>

    @if($errors->all() )

        @foreach( $errors->all() as $error )

            <div class="alert alert-danger text-center">
                {{ $error  }}
            </div>

        @endforeach

    @elseif( session()->has('success') )

        <div class="alert alert-success text-center">
            {{ session()->get('success') }}
        </div>

    @endif

    <form action="{{ route('user.handleNew') }}" method="POST">
        @csrf

        <div class="form-group">
            <label for="inputName">Nome</label>
            <input
                type="text"
                class="form-control"
                id="inputName"
                aria-describedby="nameHelp"
                placeholder="Digite o nome do funcionário"
                required
                name="name"
            >
            <small id="nameHelp" class="form-text text-muted">O nome deve ser completo, contento nome e sobrenome</small>
        </div>

        <div class="form-group">
            <label for="inputCPF">CPF:</label>
            <input
                type="text"
                class="form-control cpf"
                id="inputCPF"
                aria-describedby="cpfHelp"
                placeholder="Digite o CPF do funcionário"
                name="cpf"
                required
            >
            <small id="cpfHelp" class="form-text text-muted">Ex: 123.456.789-10</small>
        </div>

        <div class="form-group">
            <label for="inputCPF">E-mail:</label>
            <input
                type="email"
                class="form-control"
                id="inputEmail"
                aria-describedby="emailHelp"
                placeholder="Digite o Email do funcionário"
                name="email"
                required
            >
            <small id="emailHelp" class="form-text text-muted">Ex: exemplo@ticto.com.br</small>
        </div>

        <div class="form-group">
            <label for="inputOccupation">Cargo:</label>

            <select class="form-control" name="occupation" required aria-describedby="occupationHelp">
                <option disabled selected>Selecione uma opção</option>
                @foreach( $occupations as $occupation )

                    <option value="{{$occupation->id}}"> {{$occupation->description}} </option>

                @endforeach

            </select>

            <small id="occupationHelp" class="form-text text-muted">Ex: exemplo@ticto.com.br</small>
        </div>

        <div class="form-group">
            <label for="inputBirthday">Data de Nascimento:</label>
            <input
                type="date"
                class="form-control"
                id="inputBirthday"
                aria-describedby="birthdayHelp"
                placeholder="Digite o Email do funcionário"
                name="birthday"
                required
            >
            <small id="birthdayHelp" class="form-text text-muted">Ex: exemplo@ticto.com.br</small>
        </div>

        <div class="form-group">
            <label for="inputCEP">CEP:</label>
            <input
                type="text"
                class="form-control cep"
                id="inputCEP"
                aria-describedby="cepHelp"
                placeholder="Digite o CEP"
                name="cep"
                required
                onkeyup="address.validateCEP(this)"
            >
            <small id="cepHelp" class="form-text text-muted">Ex: 39801-500</small>
        </div>

        <div class="form-group">
            <label for="inputDescription">Logradouro:</label>
            <input
                type="text"
                class="form-control"
                id="inputDescription"
                aria-describedby="descriptionHelp"
                placeholder="Digite o nome do logradouro"
                name="description"
                required
                disabled
            >
            <small id="descriptionHelp" class="form-text text-muted">Ex: Av. dos Laravel Lovers</small>
        </div>

        <div class="form-group">
            <label for="inputNumber">Número:</label>
            <input
                type="text"
                class="form-control"
                id="inputNumber"
                aria-describedby="numberHelp"
                placeholder="Digite o número"
                name="number"
                required
                disabled
            >
            <small id="numberHelp" class="form-text text-muted">Ex: 2374, AP. 301</small>
        </div>

        <div class="form-group">
            <label for="inputComplement">Complemento:</label>
            <input
                type="text"
                class="form-control"
                id="inputComplement"
                aria-describedby="complementHelp"
                placeholder="Digite o Complemento"
                name="complement"
                required
                disabled
            >
            <small id="complementHelp" class="form-text text-muted">Ex: Próximo a Rua dos Javeiros :)</small>
        </div>

        <div class="form-group">
            <label for="inputNeighborhood">Bairro:</label>
            <input
                type="text"
                class="form-control"
                id="inputNeighborhood"
                aria-describedby="neighborhoodHelp"
                placeholder="Digite o Complemento"
                name="neighborhood"
                required
                disabled
            >
            <small id="neighborhoodHelp" class="form-text text-muted">Ex: Bairro do Ticto :)</small>
        </div>

        <div class="form-group">
            <label for="inputCity">Cidade:</label>
            <input
                type="text"
                class="form-control"
                id="inputCity"
                aria-describedby="cityHelp"
                placeholder="Digite o nome da Cidade"
                name="city"
                required
                disabled
            >
            <small id="cityHelp" class="form-text text-muted">Ex: Cidade do PHP</small>
        </div>

        <div class="form-group">
            <label for="inputUf">UF:</label>
            <input
                type="text"
                class="form-control"
                id="inputUf"
                aria-describedby="ufHelp"
                placeholder="Digite a UF"
                name="uf"
                required
                disabled
            >
            <small id="ufHelp" class="form-text text-muted">Ex: MG / SP / RJ / RN / RS</small>
        </div>

        <div class="text-center">
            <button type="submit" class="btn btn-primary">
                <i class="fas fa-fw fa-save"></i>&nbsp;Cadastrar
            </button>
        </div>

    </form>

    <br>

</div>

<script src="{{asset('js/aplicacao/pages/User/Address.js')}}"></script>

@include('components.footer.footer')
