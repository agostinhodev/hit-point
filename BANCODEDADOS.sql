-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 20-Nov-2020 às 01:25
-- Versão do servidor: 10.4.16-MariaDB
-- versão do PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `ticto`
--
CREATE DATABASE IF NOT EXISTS `ticto` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ticto`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `addresses`
--

CREATE TABLE `addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user` bigint(20) UNSIGNED NOT NULL,
  `cep` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complement` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `neighborhood` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uf` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `addresses`
--

INSERT INTO `addresses` (`id`, `user`, `cep`, `description`, `number`, `complement`, `neighborhood`, `city`, `uf`) VALUES
(1, 1, '39801500', 'Av. Sidônio Otoni', '2374', 'Ap. 301', 'São Jacinto', 'Teófilo Otoni', 'MG'),
(2, 2, '39801013', 'Rua Alzira Lopes de Souza', '171', 'Casa', 'Ipiranga', 'Teófilo Otoni', 'MG'),
(3, 3, '39801014', 'Rua Jader Ferreira Barranco', '10', 'Casa', 'Ipiranga', 'Teófilo Otoni', 'MG'),
(4, 4, '39801015', 'Rua Nasser Badim', '10', 'Casa', 'Ipiranga', 'Teófilo Otoni', 'MG');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clocks`
--

CREATE TABLE `clocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user` bigint(20) UNSIGNED NOT NULL,
  `type` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Caso o funcionário queira deixar uma justificativa da sua saída. Não é obrigatório, entretanto caso ele saia mais cedo, será cobrado posteriormente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `clocks`
--

INSERT INTO `clocks` (`id`, `user`, `type`, `created_at`, `notes`) VALUES
(1, 1, 1, '2020-11-16 11:10:10', NULL),
(2, 1, 2, '2020-11-16 14:10:10', NULL),
(3, 1, 3, '2020-11-16 16:10:10', NULL),
(4, 1, 4, '2020-11-16 20:10:10', NULL),
(5, 1, 5, '2020-11-16 17:10:10', 'Precisei levar meu cachorro ao veterinário'),
(6, 2, 1, '2020-11-17 11:10:10', NULL),
(7, 2, 2, '2020-11-17 14:10:10', NULL),
(8, 2, 3, '2020-11-17 16:10:10', NULL),
(9, 2, 4, '2020-11-17 20:10:10', NULL),
(10, 2, 5, '2020-11-17 17:10:10', 'Precisei ir ao dentista'),
(11, 3, 1, '2020-11-18 11:10:10', NULL),
(12, 3, 2, '2020-11-18 14:10:10', NULL),
(13, 3, 3, '2020-11-18 16:10:10', NULL),
(14, 3, 4, '2020-11-18 20:10:10', NULL),
(15, 3, 5, '2020-11-18 17:10:10', 'Precisei ir ao médico'),
(16, 4, 1, '2020-11-19 11:10:10', NULL),
(17, 4, 2, '2020-11-19 14:10:10', NULL),
(18, 4, 3, '2020-11-19 16:10:10', NULL),
(19, 4, 4, '2020-11-19 20:10:10', NULL),
(20, 4, 5, '2020-11-19 17:10:10', 'Fui assistir a apresentação na escola da minha filha. O gerente liberou');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clock_types`
--

CREATE TABLE `clock_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `clock_types`
--

INSERT INTO `clock_types` (`id`, `description`) VALUES
(1, 'ENTRADA NO SERVIÇO'),
(6, 'EXCEPCIONAL: RETORNO DE OUTROS FINS'),
(5, 'EXCEPCIONAL: SAÍDA PARA OUTROS FINS'),
(4, 'FIM DE EXPEDIENTE'),
(3, 'RETORNO DO ALMOÇO'),
(2, 'SAÍDA PARA ALMOÇO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_11_18_121416_create_occupations_table', 1),
(4, '2020_11_18_123435_create_users_table', 1),
(5, '2020_11_18_124131_create_addresses_table', 1),
(6, '2020_11_18_125355_create_clock_types_table', 1),
(7, '2020_11_18_125427_create_clocks_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `occupations`
--

CREATE TABLE `occupations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `occupations`
--

INSERT INTO `occupations` (`id`, `description`) VALUES
(2, 'BackEnd Developer'),
(3, 'FrontEnd Developer'),
(4, 'FullCycle Developer'),
(1, 'FullStack Developer'),
(6, 'Software Engineer'),
(5, 'UI/UX Designer');

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `level` tinyint(4) NOT NULL DEFAULT 0,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpf` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `occupation` bigint(20) UNSIGNED NOT NULL,
  `birthday` date NOT NULL,
  `createdBy` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `isDeleted`, `level`, `name`, `cpf`, `email`, `password`, `occupation`, `birthday`, `createdBy`) VALUES
(1, 0, 1, 'Agostinho Celestino Barbosa Neto', '18347641064', 'agostinho@ticto.com.br', '$2y$10$SZY7tC3x/kUKntEGzacfUeBiS7WcHbuqsAPA6Ntj9OS3YRXG3wIEy', 1, '1997-12-31', NULL),
(2, 0, 0, 'Arthur Martins Prates', '15820999010', 'arthur@ticto.com.br', '$2y$10$SZY7tC3x/kUKntEGzacfUeBiS7WcHbuqsAPA6Ntj9OS3YRXG3wIEy', 3, '1998-12-17', 1),
(3, 0, 0, 'Mateus Martins Moura', '60756082064', 'mateus@ticto.com.br', '$2y$10$SZY7tC3x/kUKntEGzacfUeBiS7WcHbuqsAPA6Ntj9OS3YRXG3wIEy', 6, '1995-12-12', 1),
(4, 0, 0, 'Marlon Áquila Cardoso Menegatti', '25668247031', 'marlon@ticto.com.br', '$2y$10$SZY7tC3x/kUKntEGzacfUeBiS7WcHbuqsAPA6Ntj9OS3YRXG3wIEy', 5, '1994-12-13', 1);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addresses_cep_index` (`cep`),
  ADD KEY `addresses_neighborhood_index` (`neighborhood`),
  ADD KEY `addresses_city_index` (`city`),
  ADD KEY `addresses_uf_index` (`uf`),
  ADD KEY `addresses_user_foreign` (`user`);

--
-- Índices para tabela `clocks`
--
ALTER TABLE `clocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clocks_user_foreign` (`user`),
  ADD KEY `clocks_type_foreign` (`type`);

--
-- Índices para tabela `clock_types`
--
ALTER TABLE `clock_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `clock_types_description_unique` (`description`);

--
-- Índices para tabela `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Índices para tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `occupations`
--
ALTER TABLE `occupations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `occupations_description_unique` (`description`);

--
-- Índices para tabela `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Índices para tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_cpf_unique` (`cpf`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_isdeleted_index` (`isDeleted`),
  ADD KEY `users_level_index` (`level`),
  ADD KEY `users_createdby_index` (`createdBy`),
  ADD KEY `users_occupation_foreign` (`occupation`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `clocks`
--
ALTER TABLE `clocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de tabela `clock_types`
--
ALTER TABLE `clock_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `occupations`
--
ALTER TABLE `occupations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `clocks`
--
ALTER TABLE `clocks`
  ADD CONSTRAINT `clocks_type_foreign` FOREIGN KEY (`type`) REFERENCES `clock_types` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `clocks_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_createdby_foreign` FOREIGN KEY (`createdBy`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `users_occupation_foreign` FOREIGN KEY (`occupation`) REFERENCES `occupations` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
