class ConfirmaOperacao{

    constructor(){

        this.templateBruto     = document.getElementById('templateConfirmarOperacao').innerHTML;
        this.templateCompilado = Handlebars.compile(this.templateBruto);

        Handlebars.registerHelper('stringifyFunc', function(fn, type) {

            switch(type){

                default:
                case 'function':

                    return new Handlebars.SafeString("(" + fn.toString().replace(/\"/g,"'") + ")()");

                break;

                case 'class-method':

                    return fn;

                break;

            }


        });


    }

    abrir(funcao, type){

        let json = {

            funcao,
            type

        };

        let HTMLGerado = this.templateCompilado(json);

        modal.abrir("Confirma?", HTMLGerado);
        modal.mostrarOcultarBtnFechar(0);

        setTimeout(()=>{

            document.getElementById('btnConfirmarOperacaoSim').focus();

        }, 400);

    }

}
