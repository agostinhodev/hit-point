@include('components.header.header')

@include('components.navbar.navbar')

@include('templates.Pages.Clock.templateShowTime')


<div class="container-fluid">

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Registrar seu Ponto</h1>

    </div>

    <div class="table-responsive">

        <form action="{{ route('clock.saveNew') }}" method="POST">

            @csrf

            <div class="form-group">
                <label for="notesEventType">Selecione o Evento</label>
                <select name="type" class="form-control" required>

                    <option disabled selected>Selecione um opção</option>

                    @foreach($types as $type)

                        <option value="{{ $type->id  }}">{{$type->description}}</option>

                    @endforeach

                </select>
                <small id="emailHelp" class="form-text text-muted">O evento categoriza o seu registro</small>
            </div>


            <div class="form-group">
                <label for="notesEventType">Hora do Registro</label>
                <div id="containerTime"></div>
            </div>

            <div class="form-group">
                <label for="notesTextArea">Notas:</label>
                <textarea
                    name="notes"
                    class="form-control"
                    rows="4"
                    cols="10"
                ></textarea>
                <small id="emailHelp" class="form-text text-muted">Caso julgue necessário, utilize o campo acima para observações</small>

            </div>

            <div class="text-center">
                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-fw fa-save"></i>&nbsp;Enviar
                </button>
            </div>

        </form>

        <br>

        @if($errors->all() )

            @foreach( $errors->all() as $error )

                <div class="alert alert-danger text-center">
                    {{ $error  }}
                </div>

            @endforeach

        @elseif( session()->has('success') )

            <div class="alert alert-success text-center">
                {{ session()->get('success') }}
            </div>

        @endif

    </div>

</div>

</div>

<script src="{{asset('js/aplicacao/pages/Clock/Clock.js')}}"></script>
@include('components.footer.footer')
