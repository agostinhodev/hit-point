class QueryParams{

    isset(field){

        var url = window.location.href;

        if(
            (url.indexOf('?' + field + '=') != -1)  ||
            (url.indexOf('&' + field + '=') != -1)
        ) return true;

        return false;

    }

    getValue(field){

        if(this.isset(field)){

            let params = (new URL(document.location)).searchParams;
            let name = params.get(field);
            return name;

        } 

        return false;

    }

}