<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('user')->unsigned();
            $table->string('cep', 8);
            $table->string('description', 100);
            $table->string('number', 15);
            $table->string('complement', 30)->nullable();
            $table->string('neighborhood', 100);
            $table->string('city', 100);
            $table->string('uf', 2);

            $table->index('cep');
            $table->index('neighborhood');
            $table->index('city');
            $table->index('uf');

            //$table->timestamps();

            //Relationships
            $table->foreign('user')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT')
                ->onUpdate('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
