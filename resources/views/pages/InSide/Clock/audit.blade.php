@include('components.header.header')

@include('components.navbar.navbar')

<div class="container-fluid">

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Auditoria de Horários</h1>
    </div>
    <hr>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h5 mb-0 text-gray-800">Filtro:</h1>

    </div>
    <form action="" method="GET">

        <div class="form-group">
            <label for="inputDate01">Data Inicial:</label>
            <input
                type="date"
                class="form-control"
                id="inputDate01"
                aria-describedby="emailHelp"
                name="initial_date"
                placeholder="Informe uma data inicial"
                required
                @if( isset($dates['initial']) )

                    value="{{ $dates['initial']  }}"

                @endif

            >
            <small id="emailHelp" class="form-text text-muted">Ex.: 01/01/2020</small>
        </div>
        <div class="form-group">
            <label for="inputDate02">Data Final:</label>
            <input
                type="date"
                class="form-control"
                id="inputDate02"
                name="final_date"
                placeholder="Informe uma data final"
                required
                @if( isset($dates['final']) )

                    value="{{ $dates['final']  }}"

                @endif

            >
            <small id="emailHelp" class="form-text text-muted">Ex.: {{ date('d/m/Y')  }}</small>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary">
                <i class="fas fa-fw fa-filter"></i>&nbsp;Filtrar
            </button>


            @if(!is_null($dates['initial']) && !is_null($dates['final']))
                <a href="{{route('clock.showAudit')}}">
                    <button type="button" class="btn btn-danger">
                        <i class="fas fa-fw fa-times-circle"></i>&nbsp;Limpar Filtros
                    </button>
                </a>

            @endif

        </div>
    </form>
    <hr>

    @if($errors->all() )

        @foreach( $errors->all() as $error )

            <div class="alert alert-danger text-center">
                {{ $error  }}
            </div>

        @endforeach

    @else

        @if(count($clocks) > 0)
            <div class="text-center">

                <strong>Total de {{ count($clocks) }} registros</strong>

            </div>
            <br>

        @endif

        <div class="table-responsive table-striped">

            <table class="table table-bordered dataTable" >
                <thead class="thead-dark">
                    <tr>

                        <th scope="col">ID:</th>
                        <th scope="col">Nome:</th>
                        <th scope="col">Cargo:</th>
                        <th scope="col">Idade:</th>
                        <th scope="col">Gerente:</th>
                        <th scope="col">Descrição:</th>
                        <th scope="col">Data e Hora:</th>
                        <th scope="col">Obs.::</th>

                    </tr>
                </thead>
                <tbody>

                    @if(count($clocks) === 0)

                        <tr>
                            <td colspan="8">

                                <div class="text-center">
                                    Nenhum registro encontrado com os critérios informados
                                </div>

                            </td>
                        </tr>

                    @else

                        @foreach( $clocks as $clock )

                            <tr>

                                <td>{{$clock->id}}</td>
                                <td>{{$clock->name}}</td>
                                <td>{{$clock->occupation}}</td>
                                <td>{{  date("Y") - date("Y", strtotime($clock->birthday)) }} anos</td>
                                <td>

                                    @if(is_null($clock->manager))

                                        <span class="badge badge-danger"><i>Não possui</i></span>

                                    @else

                                        {{$clock->manager}}

                                    @endif

                                </td>
                                <td>{{$clock->description}}</td>
                                <td>{{date("d/m/Y", strtotime($clock->created_at))}} às {{ date('H:i:s', strtotime($clock->created_at)) }}</td>
                                <td>

                                    @if(is_null($clock->notes))

                                        <span class="badge badge-info"><i>Não Consta</i></span>

                                    @else

                                        {{$clock->notes}}

                                    @endif

                                </td>

                            </tr>

                        @endforeach

                    @endif

                </tbody>
            </table>

        </div>

    @endif

</div>

@include('components.footer.footer')
