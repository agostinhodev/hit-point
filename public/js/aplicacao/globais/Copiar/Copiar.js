function Copiar(){

	this.execute = function(id_elemento){

		var elemento = document.getElementById(id_elemento);	  	

	  	if(document.body.createTextRange){
	  		// para Internet Explorer

	    	var range = document.body.createTextRange();
	    	range.moveToElementText(elemento);
	    	range.select();
	    	document.execCommand("Copy");

	    }else if(window.getSelection) {

	    	// outros browsers

		    var selection = window.getSelection();
		    var range = document.createRange();
		    range.selectNodeContents(elemento);
		    selection.removeAllRanges();
		    selection.addRange(range);
		    document.execCommand("Copy");

		}

	};

}

var Copiar = new Copiar();