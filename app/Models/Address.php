<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $table = 'addresses';
    public $timestamps = false;

    public function setCepAttribute( $value ){

        $value = preg_replace("/[^0-9]/", "", $value);

        $this->attributes['cep'] = $value;

    }
}
