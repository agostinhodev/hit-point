<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Occupation;
use App\Models\User;
use Dotenv\Validator;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Utils;

class UserController extends Controller
{
    //

    public function showInfo()
    {

        $id_occupation = auth()->user()->occupation;
        $occupation = Occupation::where('id', $id_occupation)->first();

        return view('pages.Inside.User.profile', [

            'user' => auth()->user(),
            'job' => $occupation->description

        ]);

    }

    public function updatePassword( Request $request )
    {

        if(!isset($request->currentPassword) || strlen($request->currentPassword) === 0)
            return redirect()->back()->withInput()->withErrors([
                'É necessário informar a senha atual'
            ]);

        if(!isset($request->newPassword) || strlen($request->newPassword) === 0)
            return redirect()->back()->withInput()->withErrors([
                'É necessário informar a nova senha'
            ]);

        if(!isset($request->newPasswordConfirmation) || strlen($request->newPasswordConfirmation) === 0)
            return redirect()->back()->withInput()->withErrors([
                'É necessário informar a confirmação da nova senha'
            ]);

        if(!(Hash::check( $request->currentPassword , auth()->user()->password)))
            return redirect()->back()->withInput()->withErrors([
                'A senha atual está incorreta'
            ]);

        if($request->currentPassword === $request->newPassword)
            return redirect()->back()->withInput()->withErrors([
                'A nova senha não pode ser igual a senha atual'
            ]);

        $request->validate([

            'currentPassword' => 'required',
            'newPassword' => 'required|string|min:8'

        ]);

        $user = Auth::user();
        $user->password = bcrypt( $request->newPassword );
        $user->save();

        return redirect()->back()->with('success', 'Senha atualizada com sucesso');


    }

    public function getAllUsers()
    {

        if(auth()->user()->level !==1 )
            return redirect()->route(
                'notices.hasNoPermission'
            );

        $users = DB::select(

            "SELECT

            users.id,
            users.isDeleted,
            users.level,
            users.name,
            users.cpf,
            users.email,
            users.birthday,
            occupations.description as occupation

            FROM users

            INNER JOIN occupations ON
            users.occupation = occupations.id

            WHERE users.isDeleted = 0

            ORDER BY users.name ASC"

        );

        return view('pages.InSide.User.all', [

            'users' => $users

        ]);

    }

    public function delete(Request $request)
    {

        $user = User::where('id', $request->user)->first();

        $logged_level = auth()->user()->level;
        $logged_user = auth()->user()->id;

        if($user->id == $logged_level)
            return redirect()->back()->withErrors(['Você não pode se auto-deletar']);


        if($logged_level !== 1)
            return redirect()->back()->withErrors(['Você não tem permissões para remover nenhum usuário']);

        if(!$user)
            return redirect()->back()->withErrors(['O Usuário selecionado não existe']);

        if($user->isDeleted !== 0)
            return redirect()->back()->withErrors(['O Usuário selecionado já foi deletado']);

        $user->isDeleted = 1;
        $user->save();

        return redirect()->back()->with('success', 'Usuário deletado com sucesso');

    }

    public function showEditForm( Request $request, int $id_user)
    {

        if(auth()->user()->level !==1 )
            return redirect()->route(
                'notices.hasNoPermission'
            );

        $user = User::where('id', $id_user)->first();
        $address = Address::where('user', $id_user)->first();

        $occupations = Occupation::all();

        return view('pages.InSide.User.edit', [

            'user' => $user,
            'occupations' => $occupations,
            'address' => $address

        ]);

    }

    public function showFormNew()
    {
        if(auth()->user()->level !==1 )
            return redirect()->route(
                'notices.hasNoPermission'
            );

        $occupations = Occupation::all();

        return view('pages.InSide.User.new', [

            'occupations' => $occupations

        ]);

    }

    public function handleUpdate( Request $request, int $id_user )
    {

        if(!isset($request->name) || strlen(trim($request->name)) === 0 )
            return redirect()->back()->withErrors(['Informe o nome do funcionário corretamente']);

        if(!isset($request->cpf) || strlen(trim($request->cpf)) === 0 )
            return redirect()->back()->withErrors(['Informe o cpf do funcionário corretamente']);

        if(!isset($request->email) || strlen(trim($request->email)) === 0 )
            return redirect()->back()->withErrors(['Informe o cpf do funcionário corretamente']);

        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL))
            return redirect()->back()->withErrors(['Informe o email informado é inválido']);

        if(!isset($request->occupation) || !is_numeric($request->occupation) )
            return redirect()->back()->withErrors(['Informe o cargo corretamente']);

        if(!isset($request->birthday) || strlen($request->birthday) != 10 )
            return redirect()->back()->withErrors(['Informe a data de nascimento corretamente']);

        //Validação do Endereço

        if(!isset($request->address_id) || strlen(trim($request->address_id)) === 9 )
            return redirect()->back()->withErrors(['Informe o id do endereço corretamente']);

        if(!isset($request->cep) || strlen(trim($request->cep)) !== 9 )
            return redirect()->back()->withErrors(['Informe o CEP corretamente']);

        if(!isset($request->description) || strlen(trim($request->description)) === 0 )
            return redirect()->back()->withErrors(['Informe o logradouro corretamente']);

        if(!isset($request->number) || strlen(trim($request->number)) === 0 )
            return redirect()->back()->withErrors(['Informe o número corretamente']);

        if(!isset($request->complement) || strlen(trim($request->complement)) === 0 )
            return redirect()->back()->withErrors(['Informe o complemento corretamente']);

        if(!isset($request->neighborhood) || strlen(trim($request->neighborhood)) === 0 )
            return redirect()->back()->withErrors(['Informe o bairro corretamente']);

        if(!isset($request->city) || strlen(trim($request->city)) === 0 )
            return redirect()->back()->withErrors(['Informe a cidade corretamente']);

        if(!isset($request->uf) || strlen(trim($request->uf)) === 0 )
            return redirect()->back()->withErrors(['Informe a UF corretamente']);

        $user = User::where('id', $id_user)->first();
        $address = Address::where('id', $request->address_id)->first();

        $totalModified = 0;

        if(

            $request->name != $user->name ||
            $request->cpf != $user->cpf ||
            $request->email != $user->email ||
            $request->occupation != $user->occupation ||
            $request->birthday != $user->birthday


        ){



            $user->name = trim($request->name);
            $user->email = trim($request->email);
            $user->occupation = $request->occupation;
            $user->birthday = $request->birthday;

            try{

                $user->cpf = trim($request->cpf);

            }catch(\Exception $e){

                return redirect()->back()->withErrors([$e->getMessage()]);

            }

            $userValidateCpf = User::where('cpf', $user->cpf)->first();
            $userValidateEmail = User::where('email', $request->email)->first();

            if($userValidateCpf->id !== $user->id)
                return redirect()->back()->withErrors(['O CPF informado já foi cadastrado para outro usuário']);

            if($userValidateEmail->id !== $user->id)
                return redirect()->back()->withErrors(['O email informado já foi cadastrado para outro usuário']);

            $user->save();
            $totalModified++;

        }

        if(

            $request->cep != $address->cep ||
            $request->description != $address->description ||
            $request->number != $address->number ||
            $request->neighborhood != $address->neighborhood ||
            $request->city != $address->city ||
            $request->uf != $address->uf

        ){

            $address->cep = $request->cep;
            $address->description = $request->description;
            $address->number = $request->number;
            $address->complement = $request->complement;
            $address->neighborhood = $request->neighborhood;
            $address->city = $request->city;
            $address->uf = $request->uf;

            $address->save();
            $totalModified++;

        }

        if($totalModified === 0)
            return redirect()->back()->withErrors(['Nenhuma alteração foi executada']);

        return redirect()->back()->with('success', 'Funcionário atualizado com sucesso');

    }

    public function handleNew( Request $request )
    {

        //Validação dos dados cadastrais

        if(!isset($request->name) || strlen(trim($request->name)) === 0 )
            return redirect()->back()->withErrors(['Informe o nome do funcionário corretamente']);

        if(!isset($request->cpf) || strlen(trim($request->cpf)) === 0 )
            return redirect()->back()->withErrors(['Informe o CPF do funcionário corretamente']);

        if(!isset($request->email) || strlen(trim($request->email)) === 0 )
            return redirect()->back()->withErrors(['Informe o email do funcionário corretamente']);

        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL))
            return redirect()->back()->withErrors(['O Email informado é inválido']);

        if(!isset($request->occupation) || !is_numeric($request->occupation) )
            return redirect()->back()->withErrors(['Informe o cargo corretamente']);

        if(!isset($request->birthday) || strlen($request->birthday) != 10 )
            return redirect()->back()->withErrors(['Informe a data de nascimento corretamente']);

        //Validação do Endereço

        if(!isset($request->cep) || strlen(trim($request->cep)) !== 9 )
            return redirect()->back()->withErrors(['Informe o CEP corretamente']);

        if(!isset($request->description) || strlen(trim($request->description)) === 0 )
            return redirect()->back()->withErrors(['Informe o logradouro corretamente']);

        if(!isset($request->number) || strlen(trim($request->number)) === 0 )
            return redirect()->back()->withErrors(['Informe o número corretamente']);

        if(!isset($request->neighborhood) || strlen(trim($request->neighborhood)) === 0 )
            return redirect()->back()->withErrors(['Informe o bairro corretamente']);

        if(!isset($request->city) || strlen(trim($request->city)) === 0 )
            return redirect()->back()->withErrors(['Informe a cidade corretamente']);

        if(!isset($request->uf) || strlen(trim($request->uf)) === 0 )
            return redirect()->back()->withErrors(['Informe a UF corretamente']);

        //Persiste os dados do usuário
        $password = uniqid();

        $user = new User();
        $user->name = trim($request->name);
        $user->password= Hash::make($password);
        $user->email = trim($request->email);
        $user->occupation = $request->occupation;
        $user->birthday = $request->birthday;
        $user->createdBy = auth()->user()->id;

        try{

            $user->cpf = $request->cpf;

        }catch(\Exception $e){

            return redirect()->back()->withErrors([$e->getMessage()]);

        }

        $userValidateCpf = User::where('cpf', $user->cpf)->first();
        $userValidateEmail = User::where('email', $request->email)->first();

        if($userValidateCpf->id !== $user->id)
            return redirect()->back()->withErrors(['O CPF informado já foi cadastrado para outro usuário']);

        if($userValidateEmail->id !== $user->id)
            return redirect()->back()->withErrors(['O email informado já foi cadastrado para outro usuário']);

        $user->save();

        //Persiste os dados endereço
        $address = new Address();
        $address->user = $user->id;
        $address->cep = $request->cep;
        $address->description = $request->description;
        $address->number = $request->number;
        $address->complement = $request->complement;
        $address->neighborhood = $request->neighborhood;
        $address->city = $request->city;
        $address->uf = $request->uf;

        $address->save();

        return redirect()->back()->with('success', "Funcionário cadastrado com sucesso. Informe a nova senha ao funcionário para que ele obtenha acesso: $password");

    }

}
