class Clock{

    constructor() {

        this.showTime();

    }


    showTime(){

        let rawTemplate = document.getElementById("templateShowTime").innerHTML;
        let compiledTemplate = Handlebars.compile(rawTemplate);
        let containerTime = document.getElementById('containerTime');
        let generatedHTML = null;

        this.write(generatedHTML, compiledTemplate, containerTime);

        setInterval( ()=>{

            this.write(generatedHTML, compiledTemplate, containerTime)

        }, 1000 );

    }

    formatTime(time){

        if(time < 10)
            return `0${time}`;

        return time;

    }

    write(generatedHTML, compiledTemplate, containerTime){


        let date = new Date();

        let json = {};

        let hours = this.formatTime( date.getHours() );
        let minutes = this.formatTime( date.getMinutes() );
        let seconds = this.formatTime( date.getSeconds() );


        json = {

            time: `${hours}:${minutes}:${seconds}`

        };

        generatedHTML    = compiledTemplate( json );
        containerTime.innerHTML = generatedHTML;

    }

}

const clock = new Clock();
