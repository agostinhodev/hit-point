<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clock extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'clocks';
    protected $fillable = [

        'user', 'type', 'notes'

    ];


}
