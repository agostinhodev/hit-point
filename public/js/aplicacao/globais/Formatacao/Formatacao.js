class Formatacao{

    dataBDparaDataBR(data) {

        //data entrada = "2019-02-04 12:11:13"

        if(data == null || data == '' || data.length < 10) return null;

        let arr  = data.split(" ");
        let dia      = arr[0].split("-");
        let temp = dia[0];
        dia[0]   = dia[2];
        dia[2]   = temp;

        if(typeof arr[1] != "undefined"){

            data = dia[0] + "/" + dia[1] + "/" +  dia[2] + " às " + arr[1];

        } else {

            data = dia[0] + "/" + dia[1] + "/" +  dia[2];

        }

        //Saída 04/02/2019 ás 12:11:13
        return data;
    }

    valorBRLparaUSD(valor){

        valor = valor.replace(/\./g,'');
        valor = valor.replace(",", ".");
        valor = parseFloat(valor);
        valor = valor.toFixed(2);
        valor = parseFloat(valor);
        return valor;

    }

    valorUSDparaBRL(valor){

        var num = new NumberFormat();
        num.setInputDecimal('.');
        num.setNumber(valor); // obj.value é '-1000.24'
        num.setPlaces('2', false);
        num.setCurrencyValue('');
        num.setCurrency(true);
        num.setCurrencyPosition(num.LEFT_OUTSIDE);
        num.setNegativeFormat(num.LEFT_DASH);
        num.setNegativeRed(false);
        num.setSeparators(true, '.', ',');

        let total_final  = num.toFormatted();
        return total_final;

    };

    convertCEPBDtoBR( cep ){

        if(cep.length !== 8)
            throw new Error('O CEP precisa ter ao menos 8 dígitos');

        let cep1 = cep.slice(0, 5);

        if(cep1.length !== 5)
            throw new Error('O Tamanho da primeira parte do cep precisa ter exatamente 5 caracteres');

        let cep2 = cep.slice(5, 8);

        if(cep2.length !== 3)
            throw new Error('O Tamanho da primeira parte do cep precisa ter exatamente 3 caracteres');

        cep = `${cep1}-${cep2}`;

        return cep;

    }

}
