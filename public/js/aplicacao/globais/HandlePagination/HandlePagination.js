class HandlePagination{

    constructor() {
        this.templatePagination = document.getElementById('templatePagination');
        this.container = null;
        this.content = null;
        this.nomeClasse = null;
    }

    setContent( content ){


        this.content = content;
    }

    setNomeClasse(nomeClasse){
        this.nomeClasse = nomeClasse;
        this.content.nomeClasse = this.nomeClasse;
    }

    setContainer( container ){
        this.container = document.getElementById(container);
    }

    show(){

        let templateCompilado    = Handlebars.compile(this.templatePagination.innerHTML);
        this.container.innerHTML = templateCompilado(this.content);
    }

}
