class Empresa {

    constructor() {

        this.containerNomeEmpresa = document.getElementById('containerNomeEmpresa');
        this.templateEmpresa      = document.getElementById('templateEmpresa');

        this.buscar();
    }

    buscar(){

        let handleRequest = new HandleRequest();

        handleRequest.process({

            headers:{
                route: 'empresa'
            },
            params: {
            },
            method: 'GET',
            responseType: 'contentMessage',
            template: this.templateEmpresa.id,
            container: this.containerNomeEmpresa.id,
            callbackSuccess: ()=>{

                let response = handleRequest.response;

                if(typeof response.empresa.nomeFantasia !== 'undefined'){

                    document.title = response.empresa.nomeFantasia;

                } else{

                    document.title = response.empresa.nomeEmpresarial;

                }

                if(document.getElementById('containerImagemFundo'))
                    document.getElementById('containerImagemFundo').style.backgroundImage=`url('${response.empresa.logomarca}')`;

            },
            callbackError: null

        });

    }

}

const empresa = new Empresa();
