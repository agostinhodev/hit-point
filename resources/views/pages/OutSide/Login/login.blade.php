@include('components.header.header')

<div class="container">

    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">

                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">

                                        <div class="text-center">
                                            ACESSO ADMINISTRATIVO
                                        </div>

                                    </h1>
                                    <h5>V1.0.0</h5>
                                </div>
                                <form
                                    class="user"
                                    method="POST"
                                    action="{{route('login.handleProcess')}}"

                                >

                                    <input
                                        type="hidden"
                                        id="token"
                                        name="google_recaptcha_token"
                                        required
                                        readonly>

                                    @csrf

                                    <div class="form-group">
                                        <input
                                            type="email"
                                            class="form-control form-control-user"
                                            name="email"
                                            aria-describedby="emailHelp"
                                            placeholder="E-mail"
                                            required
                                            autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input
                                            type="password"
                                            class="form-control form-control-user"
                                            name="password"
                                            placeholder="Senha"
                                            required>
                                    </div>

                                    <button
                                        class="btn btn-primary btn-user btn-block"
                                        id="btnLogin">
                                        Login
                                    </button>

                                </form>

                                @if($errors->all() )

                                    <br>

                                    @foreach( $errors->all() as $error )

                                        <div class="alert alert-danger text-center">
                                            {{ $error  }}
                                        </div>

                                    @endforeach

                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>


<script src="https://www.google.com/recaptcha/api.js?render=6LckeOQZAAAAAKJ62sWnmU3fEEp-jgaxkiDbETxk"></script>
<script>

    function getGoogleRecaptchaToken(){

        grecaptcha.ready(function() {
            grecaptcha.execute('6LckeOQZAAAAAKJ62sWnmU3fEEp-jgaxkiDbETxk', {action: 'login'}).then(function(token) {

                document.getElementById('token').value = token;

            });
        });

    }

    window.onload = getGoogleRecaptchaToken();

</script>

@include('components.footer.footer')
