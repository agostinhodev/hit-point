@include('components.header.header')

@include('components.navbar.navbar')

<div class="container-fluid">

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Funcionários</h1>

    </div>

    @if($errors->all() )

        @foreach( $errors->all() as $error )

            <div class="alert alert-danger text-center">
                {{ $error  }}
            </div>

        @endforeach

    @elseif( session()->has('success') )

        <div class="alert alert-success text-center">
            {{ session()->get('success') }}
        </div>

    @endif

    <br>

    <div class="table-responsive table-striped">

        <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Nome:</th>
                    <th scope="col">CPF:</th>
                    <th scope="col">E-mail:</th>
                    <th scope="col">Data Nascimento:</th>
                    <th scope="col">Cargo:</th>
                    <th scope="col" colspan="2">
                        <div class="text-center">Ações:</div>
                    </th>

                </tr>
            </thead>
            <tbody>

                @foreach( $users as $user )

                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ \App\Http\Controllers\Validation::mask($user->cpf, '###.###.###-##')  }}</td>
                        <td>{{ $user->email  }}</td>
                        <td>
                            {{ date("d/m/Y", strtotime($user->birthday)) }}
                            ({{ date("Y") - date("Y", strtotime($user->birthday))  }} anos)
                        </td>
                        <td>{{ $user->occupation }}</td>
                        <td>

                            <div>

                                <form action="{{route('user.delete')}}" id="remove_user_{{$user->id}}" method="POST">

                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="user" value="{{$user->id}}">

                                </form>

                            </div>

                            <div class="text-center">

                                @if ( $user->id === auth()->user()->id )

                                    <span class="badge badge-warning">Você não pode<br>se autoremover</span>

                                @else

                                    <button
                                        class="btn btn-danger"
                                        onclick="user.confirmDelete({{$user->id}})"
                                    >
                                        <i class="fas fa-fw fa-user-minus"></i>&nbsp;Remover
                                    </button>

                                @endif

                            </div>

                        </td>
                        <td>
                            <div class="text-center">
                                <a href="{{route('user.showEditForm', ['id_user' => $user->id])}}">
                                    <button class="btn btn-info">
                                        <i class="fas fa-fw fa-edit"></i>&nbsp;Editar
                                    </button>
                                </a>
                            </div>
                        </td>

                    </tr>

                @endforeach

            </tbody>
        </table>



    </div>

</div>

<script src="{{asset('js/aplicacao/pages/User/User.js')}}"></script>

@include('components.footer.footer')
