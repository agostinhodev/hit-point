<?php

namespace App\Http\Controllers;

use App\Models\Clock;
use App\Models\ClockTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClockController extends Controller
{
    //
    public function showFormRegister()
    {

        $types = ClockTypes::select('id', 'description')->orderBy('id')->get();

        return view('pages.InSide.Clock.clock', [

            'types' => $types

        ]);

    }

    public function showAudit( Request $request ){

        if(auth()->user()->level !==1 )
            return redirect()->route(
                'notices.hasNoPermission'
            );

        /*
         * Author: Agostinho Neto
         * Date: 19 Nov, 2020
         * Description: Explicação da query SQL
         * Notes: Não foi utilizado o Eloquent para esta consulta pois foi solicitado que fosse construído sem o referido.
         * Explicando a Query:
            #1: Existe a projeção das colunas:
                .id,
                .name,
                .occupation(cargo),
                .manager(gerente),
                .description (a descrição do ponto registrado)
                .created_at(horário do registro)
                .notes: caso o usuário deseje inserir alguma obs

            #2: A tabela projetada foi a clocks, pois é a tabela principal desta query: é onde estão, verdadeiramente,
                os registros de nosso interesse.

            #3: Foi feito um INNER JOIN com a tabela clock_types, pois sempre que existir um registro de ponto,
                ele deve ter, obrigatoriamente, um tipo.

            #4: Foi realizado um INNER JOIN com a tabela users, pois, da mesma maneira da situação anterior, um registro
                de ponto possui o usuário que o registrou.

            #5: Foi realizado um INNER JOIN com a tabela occupations para indicar qual o cargo daquele usuário que registrou o
                ponto.

            #5: Ponto importante a destacar: No último relacionamento foi utilizado LEFT JOIN. O motivo dessa utilização dá-se-á
                mediante a necessidade de mostrar o gestor (manager) daquele usuário que registrou o ponto. Porém, imagine um cenário em
                que o gestor também precise registrar o seu ponto. Em nosso sistema, um gestor não está subordinado a ninguém,
                somente possui subordinados, portanto, se fosse utilizada um relacionamente com INNER, os pontos que o gestor registrou
                para si próprio não seriam considerados na listagem, dessa forma, utilizando-se o LEFT JOIN que, imperativamente, irá manter
                os registros da tabela principal.

            #6: Por fim, um simples ORDER BY que ordernará os registros pelo nome do funcionaário e, em seguida, pela data do registro

        */

        if( isset($request->initial_date) && isset($request->final_date) ){

            if(strtotime($request->initial_date) > strtotime($request->final_date))
                return redirect()->back()->withErrors([
                    'A data inicial não pode ser maior do que a data final'
                ]);

            $clocks = DB::select(

                "SELECT

                clocks.id,
                users.name,
                occupations.description as occupation,
                manager.name as manager,
                clock_types.description,
                clocks.created_at,
                clocks.notes,
                users.birthday

                FROM clocks

                INNER JOIN clock_types ON
                clocks.type = clock_types.id

                INNER JOIN users ON
                clocks.user = users.id

                INNER JOIN occupations ON
                users.occupation = occupations.id

                LEFT JOIN users manager ON
                users.createdBy = manager.id

                WHERE

                DATE(clocks.created_at) BETWEEN :initial_date AND :final_date

                ORDER BY users.name ASC, clocks.created_at ASC",

                [

                    'initial_date' => $request->initial_date,
                    'final_date' => $request->final_date

                ]

            );

        } else {

            $clocks = DB::select("SELECT

            clocks.id,
            users.name,
            occupations.description as occupation,
            manager.name as manager,
            clock_types.description,
            clocks.created_at,
            clocks.notes,
            users.birthday

            FROM clocks

            INNER JOIN clock_types ON
            clocks.type = clock_types.id

            INNER JOIN users ON
            clocks.user = users.id

            INNER JOIN occupations ON
            users.occupation = occupations.id

            LEFT JOIN users manager ON
            users.createdBy = manager.id

            ORDER BY users.name ASC, clocks.created_at ASC");

        }

        return view('pages.InSide.Clock.audit', [

            'clocks' => $clocks,
            'dates' =>[

                'initial' => $request->initial_date,
                'final' =>$request->final_date

            ]

        ]);

    }

    public function save( Request $request ){

        if(!isset($request->type) || !is_numeric($request->type))
            return redirect()->back()->withInput()->withErrors([
                'É necessário informar tipo de evento'
            ]);

        if(!isset($request->notes) && strlen(trim($request->notes)))
            return redirect()->back()->withInput()->withErrors([
                'É necessário enviar as notas corretamentes'
            ]);

        $clock = new Clock();
        $clock->user = auth()->user()->id;
        $clock->type = $request->type;
        $clock->notes = $request->notes;
        $clock->save();

        return redirect()->back()->with('success', 'Operação executada com sucesso');

    }

}
