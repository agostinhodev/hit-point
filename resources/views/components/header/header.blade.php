<!DOCTYPE html>
<html lang="pt-BR">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ticto - Registro de Ponto</title>

    <link href="{{  asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="{{  asset('css/sb-admin-2.min.css') }}" rel="stylesheet">

    <script src="{{  asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{  asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{  asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{  asset('js/sb-admin-2.min.js') }}"></script>

    <script src="{{  asset('js/aplicacao/globais/Axios/axios.min.js') }}"></script>
    <script src="{{  asset('js/aplicacao/globais/Formatacao/NumberFormat154.js') }}"></script>
    <script src="{{  asset('js/aplicacao/globais/jQuery/jquery.mask.js') }}"></script>
    <script src="{{  asset('js/aplicacao/globais/HandleMessageSuccessError/HandleMessageSuccessError.js') }}"></script>
    <script src="{{  asset('js/aplicacao/globais/HandleBars/handlebars-v4.1.0.js') }}"></script>
    <script src="{{  asset('js/aplicacao/globais/Mascara/Mascara.js') }}"></script>
    <script src="{{  asset('js/aplicacao/globais/Formatacao/Formatacao.js') }}"></script>

</head>

<body class="bg-gradient-primary">

@include('components.modal.modal')
@include('templates.ConfirmarOperacao.ConfirmarOperacao')
@include('templates.Loading.Loading')
@include('templates.MessageSuccessError.MessageSuccessError')
