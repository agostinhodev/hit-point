<script id="templateConfirmarOperacao" type="text/x-handlebars-template">

    <div class="alert alert-default" style="text-align:center;">
        <button
            class="btn btn-success"
            onclick="{{ stringifyFunc funcao type}}"
            id="btnConfirmarOperacaoSim">Sim</button>&nbsp;&nbsp;
        <button
            class="btn btn-danger"
            onclick="modal.fechar();"
            id="btnConfirmarOperacaoNao">Não</button>&nbsp;&nbsp;
    </div>

</script>
