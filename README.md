![alt text](https://i.postimg.cc/G2JYth7q/info-home.png)

# Ticto - Registro de Ponto - Processo Seletivo

Este é um simples projeto para o processo seletivo da empresa Ticto.
A proposta deste projeto é ser um sistema bem simples que tem o objetivo de registrar ponto dos funcionários pelo navegador.

# Funcionalidades

  - Permite aos usuários fazer login
  - Permite o registro do ponto, com justificativas em caso de faltas
  - Permite aos administradores cadastrarem, editarem e removerem usuários
  - Permite aos administradores terem acesso a relatórios de registro de ponto

### Tecnologias utilizadas

Estas são algumas tecnologias utilizadas para a construção deste sistema:

* Laravel: v8.15
* MariaDB: v10.4.16-MariaDB mariadb.org binary distribution
* PHP: 7.4.12 (cli) (built: Oct 27 2020 17:18:47)
* BootStrap - v4.5.0
* Handlebars (Framework JavaScript):  v4.1.0
* FontAwesome (Script de Fonte) 
* Axios: biblioteca cliente Javascript para requisições AJAX  baseado em Promise para o navegador e node.js
* jQuery: v3.5.1


### Instalação

 1. Execute `composer install `
 
 2. Executar os comandos para dar permissão nas pastas:

```sh
$ find * -type d -exec chmod 755 {} \;
$ find * -type f -exec chmod 644 {} \;
```

 1. Adicione as seguintes permissões a todo o projeto 

```sh 
$ chmod 755 ./
```
 1. Verificar se o arquivo `.env.example` existe na raiz
 2. Renomeie `.env.example` para `.env` e coloque as suas credenciais de conexão com o MySQL
 3. Executar o comando na raiz do projeto: `php artisan key:generate`
 4. Importe o banco de dados no arquivo `BANCODEDADOS.sql` ou execute a importação das migrations com `php artisan migrate`
 5. Executar os seguintes comandos para dar permissão:
```sh 
$ chmod -R gu+w storage
$ chmod -R guo+w storage
$ php artisan cache:clear
$ php artisan serve
```
   

### Como acessar

Para acessar o sistema, basta utilizar alguns dos usuários abaixo:

| Email: | Senha: | Nível: |
| --- | --- | --- |
| agostinho@ticto.com.br | 123456 | Administrador |
| arthur@ticto.com.br | 123456 | Colaborador Comum |
| marlon@ticto.com.br | 123456 | Colaborador Comum |
| mateus@ticto.com.br | 123456 | Colaborador Comum |

## Browsers Suportados

![Chrome](https://raw.github.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png) | ![Firefox](https://raw.github.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png) | ![Safari](https://raw.github.com/alrra/browser-logos/master/src/safari/safari_48x48.png) | ![Opera](https://raw.github.com/alrra/browser-logos/master/src/opera/opera_48x48.png) | ![Edge](https://raw.github.com/alrra/browser-logos/master/src/edge/edge_48x48.png) | ![IE](https://raw.github.com/alrra/browser-logos/master/src/archive/internet-explorer_9-11/internet-explorer_9-11_48x48.png) |
--- | --- | --- | --- | --- | --- |
Mais recente ✔ | Mais recente ✔ | Mais recente ✔ | Mais recente ✔ | Mais recente ✔ | 11 ✔ |

