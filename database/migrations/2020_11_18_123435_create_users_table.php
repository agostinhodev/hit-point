<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->boolean('isDeleted')->default('0');
            $table->tinyInteger('level')->default('0');
            $table->string('name', 100);
            $table->string('cpf', 11)->unique();
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->bigInteger('occupation')->unsigned();
            $table->date('birthday');
            $table->bigInteger('createdBy')->unsigned()->nullable();

            //$table->timestamps();

            $table->index('isDeleted');
            $table->index('level');
            $table->index('createdBy');

            //Relationships
            $table->foreign('occupation')
                ->references('id')
                ->on('occupations')
                ->onDelete('RESTRICT')
                ->onUpdate('CASCADE');

            $table->foreign('createdBy')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT')
                ->onUpdate('CASCADE');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
